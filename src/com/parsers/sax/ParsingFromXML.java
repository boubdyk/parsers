package com.parsers.sax;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import com.parsers.db.*;

public class ParsingFromXML {
	
	public static final String PATH = "D:\\DBase.xml";
	
	public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, SQLException {
		File file = new File(PATH);
		
		SAXParser sax = SAXParserFactory.newInstance().newSAXParser();
		SaxHandler handler = new SaxHandler();
		sax.parse(file, handler);
		
		for (TableAnimal a : handler.getList()) {
			System.out.println(a.getIdAnimals());
			System.out.println(a.getName());
			System.out.println(a.getPopulation());
			System.out.println(a.getAverageAge());
			System.out.println(a.getPlaceOfResidence());
		}
		//DeleteDataFromTable.deleteData();
		SetDataToDataBaseFromList.setData(handler.getList());
		
	}

}
