package com.parsers.sax;

import java.util.LinkedList;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.parsers.db.TableAnimal;

public class SaxHandler extends DefaultHandler {
	
	private String element = "";
	
	private TableAnimal animal;
	private LinkedList<TableAnimal> list;
	
	public void startDocument() throws SAXException {
		list = new LinkedList<TableAnimal>();
	}

	public void endDocument() throws SAXException {
	}

	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		element = qName;
		if (element.equals("Animal")) {
			animal = new TableAnimal();
		}
		super.startElement(uri, localName, qName, attributes);
	}

	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		if (element.equals("PlaceOfResidence")) {
			list.add(animal);
		}
		element = "";
	}

	//����� ������ ����������� XML ���������.
	public void characters(char[] ch, int start, int length) 
			throws SAXException {
		switch(element) {
		case "idAnimal":
			animal.setIdAnimals(Integer.parseInt(new String(ch, start, length)));
			break;
		case "Name":
			animal.setName(new String(ch, start, length));
			break;
		case "Population":
			animal.setPopulation(Integer.parseInt(new String(ch, start, length)));
			break;
		case "AverageAge":
			animal.setAverageAge(Integer.parseInt(new String(ch, start, length)));
			break;
		case "PlaceOfResidence":
			animal.setPlaceOfResidence(new String(ch, start, length));
		case "Food":
			animal.setFood(new String(ch, start, length));
			break;
		
		}
	}
	
	public LinkedList<TableAnimal> getList() {
		return list;
	}

}
