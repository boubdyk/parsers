package com.parsers.db;

import java.sql.*;

public class ConnectionWithDataBase {

	private final String HOST = "jdbc:mysql://localhost:3306/test";
	private final String USERNAME = "root";
	private final String PASSWORD = "1qaz2wsx";

	private Connection connection;

	public ConnectionWithDataBase() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager.getConnection(HOST, USERNAME, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	public Connection getConnection() {
		return connection;
	}
}
