package com.parsers.db;

import java.sql.SQLException;
import java.sql.Statement;


public class DeleteDataFromTable {
	private static final String queryDelete = "TRUNCATE test.parsedfromxml;";
	
	public static void deleteData() throws SQLException {
		ConnectionWithDataBase worker = new ConnectionWithDataBase();
		Statement statement = worker.getConnection().createStatement();
		statement.executeUpdate(queryDelete);
	}
}
