package com.parsers.db;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class SavingDataFromDataBaseToXML {

	public void DataToXML() throws ParserConfigurationException, FileNotFoundException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		
		Element root = document.createElement("Animals");
		document.appendChild(root);
		
		int k = GetDataFromDataBaseToList.getData().size();
		//System.out.println(k);
	
		for (int i = 0; i < k; i++) {
			Element animal = document.createElement("Animal");
			root.appendChild(animal);
			Element elemId = document.createElement("idAnimal");
			elemId.setTextContent(GetDataFromDataBaseToList.getData().get(i).getIdAnimals() + "");
			Element elemName = document.createElement("Name");
			elemName.setTextContent(GetDataFromDataBaseToList.getData().get(i).getName() + "");
			Element elemPopulation = document.createElement("Population");
			elemPopulation.setTextContent(GetDataFromDataBaseToList.getData().get(i).getPopulation() + "");
			Element elemAge = document.createElement("AverageAge");
			elemAge.setTextContent(GetDataFromDataBaseToList.getData().get(i).getAverageAge() + "");
			Element elemResidence = document.createElement("PlaceOfResidence");
			elemResidence.setTextContent(GetDataFromDataBaseToList.getData().get(i).getPlaceOfResidence() + "");
			Element elemFood = document.createElement("Food");
			elemFood.setTextContent(GetDataFromDataBaseToList.getData().get(i).getFood() + "");
			
			animal.appendChild(elemId);
			animal.appendChild(elemName);
			animal.appendChild(elemPopulation);
			animal.appendChild(elemAge);
			animal.appendChild(elemResidence);	
			animal.appendChild(elemFood);
		}
		
		//System.out.println(MainDB.getData().size());
		Transformer t = TransformerFactory.newInstance().newTransformer();
		t.setOutputProperty(OutputKeys.METHOD, "xml");
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.transform(new DOMSource(document), new StreamResult(new FileOutputStream("D:\\DBase.xml")));
	}
	
	public static void main(String[] args) throws FileNotFoundException, ParserConfigurationException, TransformerException {
		SavingDataFromDataBaseToXML sdtxml = new SavingDataFromDataBaseToXML();
		sdtxml.DataToXML();
	}

}
