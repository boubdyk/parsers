package com.parsers.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class GetDataFromDataBaseToList {
	
	private static ArrayList<TableAnimal> animalsList = new ArrayList<TableAnimal>();	
	public static String query = "select * from animals";
	
	public static ArrayList<TableAnimal> getData() {
		ConnectionWithDataBase worker = new ConnectionWithDataBase();		
		try {
			Statement statement = worker.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(GetDataFromDataBaseToList.query);
			
			int counter = 0;
			while (resultSet.next()) {
				TableAnimal animal = new TableAnimal();				
				animal.setIdAnimals(resultSet.getInt("idAnimals"));
				animal.setName(resultSet.getString("Name"));
				animal.setPopulation(resultSet.getInt("Population"));
				animal.setAverageAge(resultSet.getInt("AverageAge"));
				animal.setPlaceOfResidence(resultSet.getString("PlaceOfResidence"));
				animal.setFood(resultSet.getString("Food"));
				animalsList.add(counter, animal);							
				counter ++;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		/*for (int i = 0; i < animalsList.size(); i++) {
			System.out.println("____________________________________________");
			System.out.println(animalsList.get(i).getIdAnimals());
			System.out.println(animalsList.get(i).getName());
			System.out.println(animalsList.get(i).getPopulation());
			System.out.println(animalsList.get(i).getAverageAge());
			System.out.println(animalsList.get(i).getPlaceOfResidence() + "\n");
		}*/
		return animalsList;
	}
	
//	public static void main(String[] args) {
//		MainDB.getData();
//		
//	}
}
