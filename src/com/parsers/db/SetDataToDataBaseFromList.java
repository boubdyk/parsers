package com.parsers.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;


public class SetDataToDataBaseFromList {

	public static void setData(LinkedList<TableAnimal> list) throws SQLException {
		String queryInsert = "";
		
		ConnectionWithDataBase worker = new ConnectionWithDataBase();
		
		for (int i = 0; i < list.size(); i++) {
			queryInsert ="INSERT INTO `test`.`parsedfromxml` (" + /*`idAnimals`, + */"`Name`, `Population`, " +
					"`AverageAge`, `PlaceOfResidence`, `Food`) VALUES ("/* +
					list.get(i).getIdAnimals() + ", */ + "'" + list.get(i).getName() + "', " +
					list.get(i).getPopulation() + ", " + list.get(i).getAverageAge() + ", '" + 
					list.get(i).getPlaceOfResidence() + "', '" + list.get(i).getFood() + "');";		
			//System.out.println(list.get(i).getFood());
			
			Statement statement = worker.getConnection().createStatement();
			statement.executeUpdate(queryInsert);
		}	
	}
}
