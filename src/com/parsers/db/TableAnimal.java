package com.parsers.db;

public class TableAnimal {

	private int idAnimals;
	private String name;
	private int population;
	private int averageAge;
	private String placeOfResidence;
	private String food;
	
	public TableAnimal() {		
	}

	public TableAnimal(int idAnimals, String name, int population, int averageAge,
			String placeOfResidence, String food) {
		super();
		this.idAnimals = idAnimals;
		this.name = name;
		this.population = population;
		this.averageAge = averageAge;
		this.placeOfResidence = placeOfResidence;
		this.food = food;
	}

	public String getFood() {
		return food;
	}

	public void setFood(String food) {
		this.food = food;
	}

	public int getIdAnimals() {
		return idAnimals;
	}

	public void setIdAnimals(int idAnimals) {
		this.idAnimals = idAnimals;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPopulation() {
		return population;
	}

	public void setPopulation(int population) {
		this.population = population;
	}

	public int getAverageAge() {
		return averageAge;
	}

	public void setAverageAge(int averageAge) {
		this.averageAge = averageAge;
	}

	public String getPlaceOfResidence() {
		return placeOfResidence;
	}

	public void setPlaceOfResidence(String placeOfResidence) {
		this.placeOfResidence = placeOfResidence;
	}
	

}
