package com.parsers.form;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ParsersGUI extends JFrame{
	
	String[] columnNames = { "ID", "Name", "Population", "AverageAge", "PlaceOfResidence", "Food" };
	
	Object [][] data = { { "", "", "", "", "", "" }};
	
	public ParsersGUI() {
		super("Working with DB and Parsing XML");
		JPanel mainPanel = new JPanel();
		setSize(1200, 600);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		
		mainPanel.setVisible(true);
		mainPanel.setLayout(null);
		mainPanel.setSize(1200, 600);
		
		JTable table = new JTable(data, columnNames);
		JScrollPane jScrollPanel = new JScrollPane(table);
	    table.setPreferredScrollableViewportSize(new Dimension(400,100));
	    
	    Button btn1 = new Button("ShowData");
	    btn1.setVisible(true);
	    btn1.setBounds(60, 30, 60, 30);
	   // btn1.setSize(50, 30);
	    Button btn2 = new Button("GetData");
	    btn2.setVisible(true);
	    btn2.setBounds(150, 30, 60, 30);
	    
	    Button btn3 = new Button("SaveData");
	    btn3.setVisible(true);
	    btn3.setBounds(240, 30, 60, 30);
	    
	    Button btn4 = new Button("ParseXML");
	    btn4.setVisible(true);
	    btn4.setBounds(330, 30, 60, 30);
	    
	    Button btn5 = new Button("SetData");
	    btn5.setVisible(true);
	    btn5.setBounds(420, 30, 60, 30);
	    
	    JTextArea textArea = new JTextArea(
	    	    "This is an editable JTextArea. " +
	    	    "A text area is a \"plain\" text component, " +
	    	    "which means that although it can display text " +
	    	    "in any font, all of the text is in the same font."
	    	);
	    	textArea.setFont(new Font("Serif", Font.ITALIC, 16));
	    	textArea.setLineWrap(true);
	    	textArea.setWrapStyleWord(true);
	    	textArea.setBounds(100, 100, 300, 400);
	    
	    
	    JLabel jl = new JLabel("Set Path to XML file");
	    jl.setVisible(true);
	    jl.setBounds(100, 50, 150, 20);
	    
	    JTextField tf = new JTextField();
	    tf.setVisible(true);
	    tf.setBounds(250, 50, 100, 20);
	    mainPanel.add(jl);
	    mainPanel.add(tf);
	    
	    add(jScrollPanel);
	    //add(mainPanel);
	    setVisible(true);
	}
	
	public static void main(String[] args) {
		ParsersGUI p = new ParsersGUI();
	}

}
